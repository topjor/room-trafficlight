#include <Arduino.h>


/**
 * GM_SENSOR_STATE represents the posible sensor states  
 * Use this enum for readability and allowal of sensor inversion (needed for some sensors)
 */
#ifdef INVERT_SENSOR
    enum GM_SENSOR_STATE { ACTIVE, INACTIVE };
#else
    enum GM_SENSOR_STATE { INACTIVE, ACTIVE };
#endif

/**
 * GM_SENSORS_STATE represents the full singular state of the sensors inside the gate
 */
enum GM_SENSORS_STATE {
    /** No detection */
    EMPTY,
    /** Detection in the front of the gate */
    FRONT,
    /** Detection in the middle of the gate */
    MIDDLE,
    /** Detection at the back of the gate */
    BACK
};

/**
 * GM_SM_STATE represents the posible states the gate can be in
 */
enum GM_SM_STATE {
    /** Initial state / No activity */
    EMPTY_GATE,
    /** Incomming state 1 (front) **/
    INCOMMING_S1,
    /** Incomming state 2 (middle) **/
    INCOMMING_S2,
    /** Incomming state 3 (back) **/
    INCOMMING_S3,
    /** Outgoing state 1 (back) **/
    OUTGOING_S1,
    /** Outgoing state 2 (middle) **/
    OUTGOING_S2,
    /** Outgoing state 3 (front) **/
    OUTGOING_S3
};


class gateManager {
    public:
        gateManager(int Sensor1Pin, int Sensor2Pin);
        void loop();

        int getCurrent();
        int getTotal();
        GM_SM_STATE getState();

        void resetCounters();
        void resetTotal();
        void setCurrent(int current);

        void debug();

    protected:
        void pre();
        int S1_pin;
        int S2_pin;
        bool S1_state;
        bool S1_state_last;
        unsigned long S1_trigger;
        bool S2_state;
        bool S2_state_last;
        unsigned long S2_trigger;
        int  counter_total;
        int  counter_current;
        uint8_t gate_state;

        int Sensor1Pin;
        int Sensor2Pin;

        void readSensors();
        void processSensorStates();
        void processGateState();

        bool debounce(bool current, bool last, unsigned long bounce);
        void resetDebounce(uint8_t sensor);

        GM_SENSOR_STATE Sensor1State;
        GM_SENSOR_STATE Sensor1PrevState;
        GM_SENSOR_STATE Sensor2State;
        GM_SENSOR_STATE Sensor2PrevState;

        GM_SENSORS_STATE SensorsState;
        GM_SENSORS_STATE SensorsPrevState;

        GM_SM_STATE GateState;

        int totalCounter;
        int currentCounter;
};
