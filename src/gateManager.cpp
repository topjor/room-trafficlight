#include "gateManager.h"

gateManager::gateManager(int S1_PIN, int S2_PIN) {
    this->S1_pin = S1_PIN;
    this->S2_pin = S2_PIN;
}

void gateManager::loop() {
    this->pre();

    // first sensor trigger (going in)
    if (this->gate_state == 0 && this->S1_state_last == 0 && this->S1_state == 1) {
        this->gate_state = 1;
        // deboucnce signal
        this->S1_trigger = millis();
    } else {
        // if singal stays on, keep debouncing
        if (this->gate_state == 1 && digitalRead(this->S1_pin) == 0) {
            this->S1_trigger = millis();
        }
        
        // if second sensor also triggers (while first on)
        if (this->gate_state == 1 && this->S2_state_last == 0 && this->S2_state == 1) {
            this->gate_state = 2;
            // clear debounce as we dont need it anymore here
            this->resetDebounce(1);
        } else
            // if first sensor goes out (second stays on)
            if (this->gate_state == 2 && this->S1_state_last == 1 && this->S1_state == 0) {
                this->gate_state = 3;
            } else
                // second sensor goes off (passed gate)
                if (this->gate_state == 3 && this->S2_state_last == 1 && this->S2_state == 0) {
                    // reset
                    this->gate_state = 0;
                    // increment timers
                    this->counter_current++;
                    this->counter_total++;
                } else
            if (this->gate_state == 2 && this->S2_state_last == 1 && this->S2_state == 0) {
                this->gate_state = 1;
            } else
        if (this->gate_state == 1 && this->S1_state_last == 1 && this->S1_state == 0) {
            this->gate_state = 0;
        } else

    if (this->gate_state == 0 && this->S2_state_last == 0 && this->S2_state == 1) {
        // start the passing of the person
        this->gate_state = 4;
        this->S2_trigger = millis();
    } else {
        // if singal stays on, keep debouncing
        if (this->gate_state == 4 && digitalRead(this->S2_pin) == 0) {
            this->S2_trigger = millis();
        }

        if (this->gate_state == 4 && this->S1_state_last == 0 && this->S1_state == 1) {
            this->gate_state = 5;
            this->resetDebounce(2);
        } else
            if (this->gate_state == 5 && this->S2_state_last == 1 && this->S2_state == 0) {
                this->gate_state = 6;
            } else
                if (this->gate_state == 6 && this->S1_state_last == 1 && this->S1_state == 0) {
                    this->gate_state = 0;
                    this->counter_current--;
                }
            if (this->gate_state == 5 && this->S2_state_last == 1 && this->S2_state == 0) {
                this->gate_state = 4;
            } else
        if (this->gate_state == 4 && this->S2_state_last == 1 && this->S2_state == 0) {
            this->gate_state = 0;
        }
    }
    }
}

void gateManager::pre() {
  // last state
  this->S1_state_last = this->S1_state;
  this->S2_state_last = this->S2_state;
  // current state
  this->S1_state = digitalRead(this->S1_pin);
  this->S2_state = digitalRead(this->S2_pin);
  // invert
  if (this->S1_state == 0) {
    this->S1_state = 1;
  } else {
    this->S1_state = 0;
  }
  if (this->S2_state == 0) {
    this->S2_state = 1;
  } else {
    this->S2_state = 0;
  }
  this->S1_state = debounce(this->S1_state, this->S1_state_last, this->S1_trigger);
  this->S2_state = debounce(this->S2_state, this->S2_state_last, this->S2_trigger);
}

int gateManager::getCurrent() {
    return this->counter_current;
}
int gateManager::getTotal() {
    return this->counter_total;
}

void gateManager::debug() {
    Serial.println(this->gate_state);
    Serial.print(this->S1_state);
    Serial.print(' ');
    Serial.println(this->S2_state);
}

bool gateManager::debounce(bool current, bool last, unsigned long bounce) {
    if (current == 0 && last == 1) {
        if (millis() - bounce < 700) {
            return 1;
        }
    }
    return current;
}
void gateManager::resetDebounce(uint8_t sensor) {
    if (sensor == 1) {
        this->S1_trigger = 0;
    } else
    if (sensor == 2) {
        this->S2_trigger = 0;
    }
}