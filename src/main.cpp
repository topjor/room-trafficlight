/* INFO
WC-Stoplicht program
© 10-10-2020 Mark, Jordie en Jeroen
V0.0

*/

#include <Arduino.h>
#include <DmxSimple.h>
#include "gateManager.h"
#include "LedStrip.h"

// constants
const int sensor1 = 11;//10         // the number of the 1st sensor pin
const int sensor2 = 10;         // the number of the 2nd sensor pin

gateManager gm(sensor1, sensor2);
LedStrip ledstrip(16);

int last_current = 0;

/**
 * Gate state enum
 * 0 = no sensor active
 * 1 = first sensor active first
 * 2 = both sensors active
 * 3 = last sensor active
 *
 * 4 = last sensor active first
 * 5 = both sensors active
 * 6 = first sensor active
 */

void setup() {
  Serial.begin(9600);

  /* DMX devices typically need to receive a complete set of channels
  ** even if you only need to adjust the first channel. You can
  ** easily change the number of channels sent here. If you don't
  ** do this, DmxSimple will set the maximum channel number to the
  ** highest channel you DmxSimple.write() to.
  */
  // DmxSimple.maxChannel(4);
  DmxSimple.usePin(6); //This is pin D6!

  pinMode(sensor1,INPUT);
  pinMode(sensor2,INPUT);  
}

void loop() {
  gm.loop();
  /*
    DmxSimple.write(1, brightness);
  */

//   //Sensor IN Logic
//   if (sensorInState != sensorInLastState) {
//     // if the state has changed, increment the counter
//     if (sensorInState == HIGH) {
//       // if the current state is HIGH then the sensor detecten ingoing visitor. Add 1 to counter:
//       visitorCounter++;
//     }
//     // Delay a little bit to avoid bouncing
//     delay(50);
//     }
//   sensorInLastState = sensorInState;

//   //Sensor OUT Logic
//   if (sensorOutState != sensorOutLastState) {
//     // if the state has changed, increment the counter
//     if (sensorOutState == HIGH) {
//       // if the current state is HIGH then the sensor detecten ingoing visitor. Add 1 to counter:
//       visitorCounter--;
//     }
//     // Delay a little bit to avoid bouncing
//     delay(50);
//   }
//   sensorOutLastState = sensorOutState;
  
  
//   //Visitorcounter waarde naar stoplicht DMX logic

//  if (visitorCounter<=3){
//       //DMX to green
//       DmxSimple.write(1, 0);     //R-channel
//       DmxSimple.write(2, 255);   //G-channel
//       DmxSimple.write(3, 0);     //B-channel
// }
  
// else if (visitorCounter>5){
//       // DMX to red
//       DmxSimple.write(1, 255);   //R-channel
//       DmxSimple.write(2, 255);   //G-channel
//       DmxSimple.write(3, 0);     //B-channel
// }
  
// else {
//       // Use default for orange
//       DmxSimple.write(1, 255);   //R-channel
//       DmxSimple.write(2, 127);   //G-channel
//       DmxSimple.write(3, 0);     //B-channel
//   }



      Serial.println("This cycle:");
      Serial.println(gm.getTotal());
      Serial.println(gm.getCurrent());
      
      if (last_current != gm.getCurrent()) {
        ledstrip.clear();
        for (int i = 0; i < gm.getCurrent(); i++) {
          if (i < 2) {
            ledstrip.setPixelColor(i, 0, 250, 8);
          } else
          if (i < 4) {
            ledstrip.setPixelColor(i, 250, 140, 0);
          } else {
            ledstrip.setPixelColor(i, 255, 0, 0);
          }
        }
        ledstrip.show();

        last_current = gm.getCurrent();
      }
      
      
      // gm.debug();
  delay(100);
}